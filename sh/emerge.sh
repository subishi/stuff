#!/bin/sh

#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>

# required packages: mkstage4, coreutils



if [ "$(id -u)" =  0 ]; then
	printf 'Script is now running\n'
else
	printf 'run this script as root\n' ; exit 1
fi


printf 'Starting the backup\n'
printf 'Please note that /home will not be included in the stage4\n'

sleep 3

mkdir /backup
cd /backup
mkstage4 -q -e /home -s system 
cd ~

printf 'Stage4 completed!\n'
printf 'Starting to sync & update!\n'

emerge --sync >/dev/null 2>&1
emerge -uDN @world >/dev/null 2>&1

printf 'Done!\n'
