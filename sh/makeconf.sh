#!/bin/sh 


#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>





# this should not be used in an actual work enviroment , this is just a test.
# requirements: mirrorselect, coreutils, cpuid2cpuflags

TMP="$(cpuid2cpuflags)"
CPU_FLAGS="${TMP#"CPU_FLAGS_X86: "}"



printf 'THIS WILL DELETE YOUR make.conf, IF YOU DONT WANT THIS THEN PRESS CTRL + C TO CANCEL THE SCRIPT, YOU HAVE 5 SECONDS!\n'
printf 'This will also emerge mirrorselect & cpuid2cpuflags.\n'

sleep 5

if [ "$(id -u)" =  0 ]; then
	rm -f /etc/portage/make.conf
else
	printf 'run this script as root\n' ; exit 1
fi


emerge app-portage/mirrorselect app-portage/cpuid2cpuflags>/dev/null



printf 'Valid syntax: 1,2,3 etc. (Not one, two, three etc.)\n'
printf 'How much RAM do you have?\n'
read -r ram

if [ "$ram" -ge 4 ]
then
	printf 'COMMON_FLAGS="-march=native -O2 -pipe"' > /etc/portage/make.conf
else
	printf 'COMMON_FLAGS="-march=native -O2"' > /etc/portage/make.conf
fi


printf 'CFLAGS="${COMMON_FLAGS}"' >> /etc/portage/make.conf
printf 'CXXFLAGS="${COMMON_FLAGS}"'>> /etc/portage/make.conf
printf 'FCFLAGS="${COMMON_FLAGS}"'>> /etc/portage/make.conf
printf 'FFLAGS="${COMMON_FLAGS}"' >> /etc/portage/make.conf

printf 'Type in the USE flags you want to use.\n '
read -r use

printf "USE=\"$use\"" >> /etc/portage/make.conf

printf 'What licenses would you like to accept?\n'
printf 'For more info, see https://wiki.gentoo.org/wiki/Handbook:AMD64/Full/Installation#Optional:_Configuring_the_ACCEPT_LICENSE_variable\n'
read -r license
printf "ACCEPT_LICENSE=\"$license\"" >> /etc/portage/make.conf


printf 'Valid syntax: 1,2,3 etc. (Not one, two, three etc.)\n'
printf 'How many threads would you like to use for compilation?\n'
read -r threads

printf "MAKEOPTS=\"-j$threads\"" >> /etc/portage/make.conf

printf "CPU_FLAGS_X86=\"$CPU_FLAGS\"" >> /etc/portage/make.conf


printf	'PORTDIR="/var/db/repos/gentoo"' >> /etc/portage/make.conf
printf	'DISTDIR="/var/cache/distfiles"' >> /etc/portage/make.conf
printf	'PKGDIR="/var/cache/binpkgs"'>> /etc/portage/make.conf
printf	'LC_MESSAGES=C' >> /etc/portage/make.conf

mirrorselect -i -o >> /etc/portage/make.conf

clear


printf 'Almost done! Now, tell me if your system is UEFI or BIOS for grub.\n'
printf 'Valid syntax: uefi, bios\n'
read -r bios

if [ "$bios" = uefi ]; then
	printf 'GRUB_PLATFORMS="efi-64"' >> /etc/portage/make.conf
else
	printf 'System is BIOS\n'
fi

printf 'The script is done now! Enjoy!'

