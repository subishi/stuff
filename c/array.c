/*    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>
*/


#include <stdio.h>

int main() {

int rating[10];

rating[0] = 1;
rating[1] = 2;
rating[2] = 3;
rating[3] = 4;
rating[4] = 5;
rating[5] = 6;
rating[6] = 7;
rating[7] = 8;
rating[8] = 9;
rating[9] = 10;

printf("%d/10\n", rating[1]);
}
